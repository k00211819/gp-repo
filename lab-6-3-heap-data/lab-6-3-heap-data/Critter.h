#pragma once
#ifndef CRITTER_H // #include guards used to stop the header file being possibly included multiple times leading to circular inclusions/type redefintions
#define CRITTER_H

#include <string>
#include <iostream>

using namespace std;

class Critter
{
public:
	Critter(const string& name = "", int age = 0);
	~Critter();                   //destructor prototype   
	Critter(const Critter& c);    //copy constructor prototype
	Critter& Critter::operator=(const Critter& c);  //overloaded assignment op
	void Greet() const;

private:
	string* m_pName;
	int m_Age;
};

#endif
