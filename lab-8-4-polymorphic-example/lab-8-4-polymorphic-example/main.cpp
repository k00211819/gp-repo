
#include "Boss.h"

int main()
{
	cout << "Calling Attack() on Boss object through pointer to Enemy:\n";
	Boss* pBadGuy = new Boss();
	pBadGuy->Attack();

	cout << "\n\nDeleting pointer to Enemy:\n";
	delete pBadGuy;
	pBadGuy = 0;

	Boss* pAnotherBadGuy = new Boss();
	pAnotherBadGuy->Attack();

	cout << "\n\nDeleting pointer to Enemy:\n";
	delete pBadGuy;
	pBadGuy = 0;

	getchar();

	return 0;
}
