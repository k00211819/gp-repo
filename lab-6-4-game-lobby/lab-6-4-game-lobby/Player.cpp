#include "Player.h"

Player::Player(const string& name) :
	m_Name(name),
	m_pNext(0)
{}

string Player::GetName() const
{
	return m_Name;
}

Player* Player::GetNext() const
{
	return m_pNext;
}

void Player::SetNext(Player* next)
{
	m_pNext = next;
}

ostream& operator<<(ostream& os, const Player& aPlayer)
{
	os << aPlayer.m_Name << endl;

	return os;
}

//Addplayer is inefficient, it has to cycle through complete list to find the end of the list. Add an m_tail pointer data member to
// the lobby class that always points to the last player in the linked list. Use this to add players.