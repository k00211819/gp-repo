#include "Orc.h"

Orc::Orc(int health, int speed)
{
	m_Health = health;
	m_Speed = speed;

}

void Orc::Greet() const
{
	cout << "The orc grunts hello.\n";
}

void Orc::DisplayHealth() const
{
	cout << "The orcs health is " << m_Health << endl;
}

void Orc::displaySpeed() const
{
	cout << "Orc Speed :" << m_Speed << endl;
}
