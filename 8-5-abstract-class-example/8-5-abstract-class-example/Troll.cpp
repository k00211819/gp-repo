#include "Troll.h"

Troll::Troll(int health, int speed) 
{
	m_Health = health;
	m_Speed = speed;
}

void Troll::Greet() const
{
	cout << "The Troll grunts hello.\n";
}

void Troll::displaySpeed() const
{
	cout << "Troll Speed :" << m_Speed << endl;
}
