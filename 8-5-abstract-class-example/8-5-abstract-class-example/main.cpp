#include "Orc.h"
#include "Troll.h"
#include <vector>
#include <algorithm>

int main()
{
	Creature* pOrc = new Orc();
	pOrc->Greet();
	pOrc->DisplayHealth();
	pOrc->displaySpeed();

	Creature* pTroll = new Troll();
	pTroll->Greet();
	pTroll->DisplayHealth();
	pTroll->displaySpeed();
	cout << endl << endl;


	vector<Creature*> vpCreatures;
	vpCreatures.push_back(pOrc);
	vpCreatures.push_back(pTroll);

	vector<Creature*>::iterator it;

	for (it = vpCreatures.begin(); it != vpCreatures.end(); it++)
	{
		(*it)->Greet();
		(*it)->DisplayHealth();
		(*it)->displaySpeed();
	}

	getchar();
	return 0;
}
