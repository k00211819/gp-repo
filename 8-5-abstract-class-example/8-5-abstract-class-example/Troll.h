#pragma once

#include "Creature.h"

class Troll : public Creature
{
public:
	Troll(int health = 200, int speed = 3);
	virtual void Greet() const;
	virtual void displaySpeed() const;


};