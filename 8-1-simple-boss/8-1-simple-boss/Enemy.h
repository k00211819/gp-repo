#pragma once
#ifndef ENEMY_H // #include guards used to stop the header file being possibly included multiple times leading to circular inclusions/type redefintions
#define ENEMY_H

#include <iostream>
using namespace std;

class Enemy
{
public:
	Enemy();
	void Attack() const;

protected:
	int m_Damage;
};

#endif