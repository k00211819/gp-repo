#pragma once

#ifndef BOSS_H // #include guards used to stop the header file being possibly included multiple times leading to circular inclusions/type redefintions
#define BOSS_H
#include "Enemy.h"


class Boss : public Enemy 
{
public:

	Boss();
	void SpecialAttack() const;

private:
	int m_DamageMultiplier;
};

#endif