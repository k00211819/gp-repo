#pragma once
#ifndef CRITTER_H
#define CRITTER_H

#define SLEEPTIME 10

#include <iostream>

using namespace std;

class Critter
{
public:
	Critter(int hunger = 0, int boredom = 0, int age = 0);
	void Talk();
	void Eat(int food = 4);
	void Play(int fun = 4);
	void sleep();
	void myAgeIs();
	

private:
	int m_Hunger;
	int m_Boredom;
	int m_Age;

	int GetMood() const;
	int getAge() const;
	void PassTime(int time = 1);

};


#endif


